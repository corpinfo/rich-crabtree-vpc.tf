variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}

variable "environment" {
  description = "Deploy Environment"
  type        = string
  default     = "dev"
}

variable "email" {
  description = "SNS Endpoint Email Address"
  type        = string
  default     = "jimmy.moran@rackspace.com"
}

variable "protocol" {
  description = "SNS Protocol"
  type        = string
  default     = "email"
}

variable "customer_name" {
  description = "Customer Name for this Deployment"
  type        = string
  default     = "imgmgrjimmylabz"
}

variable "ApplicationName" {
  description = "Name of the deployed application"
  type        = string
  default     = "img-mgr"
}

variable "SSH_Key" {
  description = "SSH Key to use for EC2 Instances"
  type        = string
  default     = "dev-img-mgr-key"
}

variable "ami_id" {
  description = "Amazon AMI ID Image to apply to EC2"
  type        = string
  default     = "ami-087c17d1fe0178315"
}

variable "ASGHealthCheck" {
  description = "types can be ELB or EC2"
  type        = string
  default     = "ELB"
}

variable "ASG_HC_GracePeriod" {
  description = "ASG Health Check Grace Period"
  type        = number
  default     = 300
}

#Environment variables set in respective tfvars/environment files
variable "ASGMin" {}
variable "ASGMax" {}
variable "ASGDesired" {}
variable "instance_type" {}


variable "CPUHighPolicy" {
  description = "CPU Alarm High Threshold"
  type        = string
  default     = "50"
}


variable "CPULowPolicy" {
  description = "CPU Alarm Low Threshold"
  type        = string
  default     = "15"
}
<<<<<<< HEAD


variable "instance_type" {
  description = "Type of EC2 Instance to Deploy"
  type        = string
  default     = "t2.small"
}
=======
>>>>>>> remotes/origin/ENV-dev
